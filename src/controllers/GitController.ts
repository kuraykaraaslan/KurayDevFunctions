import axios from "axios";

const GITHUB_TOKEN = process.env.GITHUB_TOKEN;
const GITLAB_TOKEN = process.env.GITLAB_TOKEN;
const GITHUB_USER = process.env.GITHUB_USER;
const GITLAB_USER = process.env.GITLAB_USER;

type ContributionDay = {
    color: string;
    contributionCount: number;
    date: string;
    weekday: number;
};

type Week = {
    contributionDays: ContributionDay[];
    firstDay: string;
};

type Weeks = Week[];

type GraphQLRes = {
    user: {
        contributionsCollection: {
            contributionCalendar: {
                weeks: Weeks;
            };
        };
    };
};

const getGithubContributions = async () => {

    return GITHUB_TOKEN;

    
}