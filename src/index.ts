import express, { Request, Response } from 'express';
import * as functions from 'firebase-functions';
import cors from 'cors';
import { RequestHandler } from 'express-serve-static-core';

const app = express();
app.use(express.json() as RequestHandler);
app.use(express.urlencoded({
    extended: true,
}) as RequestHandler);
app.use(cors({ origin: true }));

app.get('/hello', (req: Request, res: Response) => {
    res.send('Hello from Firebase!');
});

app.post('/hello', (req: Request, res: Response) => {
    const { name } = req.body;
    res.send(`Hello from Firebase, ${name}!`);
});

export const api = functions.https.onRequest(app);